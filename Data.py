import initialization

'''
PSEUDOCODE

1.DEPONUN HEP BAŞTA VE SONDA KALMASINA TÜM KOD BOYUNCA DİKKAT ET!!!!!!

fonksiyonlar:
-calculate_objective (obj funct değerini hesaplat)
-calculate_tour_length
-2-opt
-find nearest

-DATA OKUMA

-INITIALIZATION (input = distances,profits, output = route )
    -[1,1] OLACAK BİR TUR İLE BAŞLA- MUTLAKA DEPODAN ÇIKIP DEPOYA DÖNECEK, BUNLARIN HEP BAŞTA VE SONDA KALMASINA TÜM KOD BOYUNCA DİKKAT ET
    -İNİTİAL POİNT SEÇ, PROFİTİNE YA DA CENTRAL OLMASINA GÖRE
    -DİSTİNE PROFİTİNE GÖRE EN UYGUN YERE İNSERT ET (SUYUNUYA BAKABILIRSIN)
    -KAÇ CITY EKLEYENE KADAR DEVAM EDECEĞINE RANDOM KARAR VER (n/2 ve n/3 arasında olabilir)
    -bunu 3 kere tekrar et, en iyisini initial turun yap
    -tabu listesini initialize et (delete edemeyeceklerimiz)

- Move operator bul fonksiyonu:
    -INSERTION ADAYINI BUL:
        -turda olmayan her nodun (profit-mevcut turdakine en yakın uzaklık) değeri  max olan 2 nodu seç,
        random birini kendisine en yakın 2 node arasına insert et

    -DELETION ADAYINI BUL:
        -turda olan her nodun silindiğinde (-götüreceği profit + distance kazancı) en yüksek olan 2 nodu seç,
         random birini sil

    #insertion ya da deletion yap
    -FIND best action
        objective'i yuksek olani sec
    aksiyon deletion ise:
        İNCUMBENTI GEÇİYOSA YAP,
        geçmiyosa ve deletion ise tabuda mı bak, tabuda değilse yap
    aksiyon insertion ise:
        her turlu uygula
        delete edemeyeceklerin tabu listene ekle (TABUYA NODE EKLIYORUZ), TABU TENURE 10-20 ARASI RANDOM


-2-opt uygula (first improvement) ##dilara'da hazir fonksiyon varmis :)

-if improvement: İNCUMBENTINI UPDATE ET else: continue ## main loop icinde

-until stopping criterion achieved (number of iterations=10000)


'''

def swap_2opt(route, i, k):
	'''
	swaps the endpoints of two edges by reversing a section of nodes, ideally to eliminate crossovers
	returns the new route created with a the 2-opt swap
	route - route to apply 2-opt
	i - start index of the portion of the route to be reversed
	k - index of last node in portion of route to be reversed pre: 0 <= i < (len(route) - 1) and i < k < len(route) post: length of the new route must match length of the given route
	'''
	new_route = route[0:i]
	new_route.extend(reversed(route[i:k + 1]))
	new_route.extend(route[k+1:])
	return new_route

def run_2opt(route,distances,profits):
    improvement = True
    best_route = route
    best_distance = self.length(route, D)
    while improvement:
        improvement = False
        for i in range(len(best_route) - 1):
            for k in range(i + 1, len(best_route)):
                new_route = self.swap_2opt(best_route, i, k)
                new_distance = self.length(new_route, D)
                if new_distance < best_distance:
                    best_distance = new_distance
                    best_route = new_route
                    improvement = True
                    break  # improvement found, return to the top of the while loop
            if improvement:
                break
    return best_route