from scipy.spatial import distance, distance_matrix
import pandas as pd
import numpy as np
import time
import random
import os
# os.chdir('FinalProject/517proje')
highlow = "LP"
N = 101 # 51, 76 or 101
# Iteration Count
iterations = 10000
fname = "dataset-" + highlow + ".xls"
dat = pd.read_excel(fname, sheetname = "eil"+str(N), header = None, index_col = 0)
dat.columns = ['x', 'y', 'profit']
starttime = time.time()


def shift(sol):  # the position of one random city is changed randomly (shift)
    pos = list(np.random.choice(len(sol), 2, replace=False))  # select two random positions from route

    mi = min(pos)
    ma = max(pos)
    pos[0] = mi  # smaller of the selected two positions
    pos[1] = ma  # larger of the selected two positions

    job = sol[pos[1]]  # take the city which is at the larger position

    for i in range(pos[1], pos[0], -1):  # shift the position of cities that are in between the selected positions
        sol[i] = sol[i - 1]

    sol[pos[0]] = job  # put the city at the smaller position

    return sol

def roulette(gaincandidates):
    max = sum(gaincandidates)
    pick = random.uniform(0, max)
    current = 0
    for idx in range(len(gaincandidates)):
        current += gaincandidates[idx]
        if current > pick:
            a = idx
            break

    return a

def tourLen(seq, dist):
    frm = seq
    to = seq[1:] + [seq[0]]
    return sum(dist[[frm, to]])

def calculateObjective(tour,dist,profits):
    cost = tourLen(tour,dist)
    profit = sum(profits[tour])
    return profit-cost

distances = distance_matrix(dat[['x','y']], dat[['x','y']], p=2) #find euclidean distance
profits = np.array(dat['profit'])

def two_opt(seq, dist_mat): #n : number of cities, dist_mat: distance matrix for cities
    n=len(seq)
    seq=seq.copy()
    delta_min = -1
    #continue until there is no improvement in tour length
    while delta_min < 0:
        delta = []
        arcs = []
        #find all possible arcs that can be replaced
        for i in range(0, n):
            for k in range(i + 2, n):
                j = (i + 1) % n
                k = k % n
                l = (k + 1) % n
                #cycling is prevented by following if conditions
                if i == l:
                    continue
                if j == k:
                    continue
                d_ik = dist_mat[seq[i],seq[k]]
                d_jl = dist_mat[seq[j],seq[l]]
                d_ij = dist_mat[seq[i],seq[j]]
                d_kl = dist_mat[seq[k],seq[l]]
                delta_current=d_ik+d_jl-d_ij-d_kl
                delta.append(delta_current)
                arcs.append([i,j,k,l])
        delta_min = min(delta)
        delta_min_i = np.argmin(delta)
        arc_min = arcs[delta_min_i]
        #for the best improvement swap nodes
        if delta_min < 0:
            seq[arc_min[1]] , seq[arc_min[2]] = seq[arc_min[2]] , seq[arc_min[1]]
    return seq

def initialization(distMatrix,profits,nstart,starttime = starttime,construction_method="nearest insert",sortType="mergesort"):
    starttime = time.time()

    #Here, a sequence for number of customers is created
    #according to given problem size
    #this is going to be used to iterate through the for loop
    lower = int(len(profits) * 0.2)
    upper = int(len(profits) * 1)
    ncustomers = np.linspace(lower,upper,nstart,dtype=int)

    profs = profits.copy()
    profs[0]=-999 #profit of the depot is set to a very low number
    tours = [[]]*nstart
    tours_after2opt = [[]] * nstart
    initial_objectives = []
    initial_objectives_after2opt = []
    for idx,ncust in enumerate(ncustomers):
        tours[idx] = [0] #tour is initialized with depot
        while len(tours[idx])<=ncust-1:
            best = []
            best_values = []
            if construction_method == 'nearest insert': #implement nearest insert algorithm
                #SELECTION STEP
                for i,j in enumerate(tours[idx]):
                    # candidate customers are sorted according to
                    # non-increasing (profit/distance) values
                    candid = (-(profs)/distMatrix[j,:]).argsort(kind=sortType)
                    # select the candidate with max (profit/distance) value, that is not already in the tour
                    best.append([x for x in candid if x not in tours[idx]][0])
                    best_values.append(profs[best[i]]/distMatrix[j,best[i]])
                selected=best[np.argmax(best_values)]
                if len(tours[idx])<=2:
                    tours[idx].append(selected)
                    continue #skip the insertion step if there is only 1 cust and the depot in the tour
                #INSERTION STEP
                temp_vertices = tours[idx]+[tours[idx][0]] #temporarily add the depot at the very end of the tour
                arc_candid = []
                #find all possible insertions and their costs=(increase in distance when added)
                for a in range(len(temp_vertices)-1):  #try all possible insertions
                    c_ij=distMatrix[temp_vertices[a], temp_vertices[a+1]]
                    c_ik=distMatrix[temp_vertices[a], selected]
                    c_kj=distMatrix[selected, temp_vertices[a+1]]
                    arc_candid.append(c_ik+c_kj-c_ij)
                index = np.argmin(arc_candid)+1
                tours[idx].insert(index, selected)

            if construction_method == 'nearest neighbor': #implement nearest neighbor algorithm
                neigh = (-(profs)/distMatrix[tours[idx][-1], :]).argsort(kind=sortType)
                neighbors = [x for x in neigh if x not in tours[idx]]  # find neighbors that have not been selected yet
                tours[idx].append(neighbors[0])

        tours[idx] = shift(tours[idx]) #add randomness to resulting sequence for diversity
        initial_objectives.append(calculateObjective(tours[idx],distMatrix,profits))
        tours_after2opt[idx] = two_opt(tours[idx],distances)
        initial_objectives_after2opt.append(calculateObjective(tours_after2opt[idx],distMatrix,profits))
    summary= pd.DataFrame(data={'initial_tour_len':ncustomers,
                                'initial_obj':initial_objectives,
                                'initial_obj_after2opt':initial_objectives_after2opt},
                          index=None)
    print(summary)
    print('Initialization time:',time.time()-starttime)
    return tours,tours_after2opt,initial_objectives,initial_objectives_after2opt

def h(seq): # a function to implement argsort on lists
    return sorted(range(len(seq)), key=seq.__getitem__)

def insertion(tour,distMatrix,profits,a): #insertion operator
    tour_new = tour.copy()
    chosen_candid = None # initialize chosen candidate which is to be inserted to the given tour
    if len(tour)!=len(distMatrix):  # prevent insertion when the tour includes all cities
        candidates = [] # initialize candidate list
        candidates = [i for i in range(N) if i not in tour] # candidates are the cities that are not yet visited
        gain_candidates = [] # list for gain parameter of the cities which is defined below

        # choose the best candidate via roullette wheel selection method
        # where the selection probabilities are proportional to
        # their contribution to the tour which is inversely propotional to distance added to the tour
        # and proportional to their profit (=profit/distance to nearest city in the tour)
        for candid in candidates: # calculate gain for each candidate city
            profit_candid = profits[candid]
            coord_candid = np.array(dat.iloc[candid][['x','y']]).reshape(1,-1)
            coord_tour = np.array(dat.iloc[tour][['x','y']])
            dist_to_tour = distance_matrix(coord_candid,coord_tour,p=2)
            gain_candid = np.max(profit_candid / dist_to_tour)
            gain_candidates.append(gain_candid)

        #candidates are chosen via roulette wheel selection method
        chosen_candid = candidates[roulette(gain_candidates)]
        #insert the chosen candidate to the tour
        temp_vertices = (tour+[tour[0]]).copy()   #add the depot to the tour
        arc_candid = []
        selected = chosen_candid

        #find all possible insertions and their costs=(increase in distance)
        #then insert where the cost is minimum
        for a in range(len(temp_vertices)-1):
            c_ij=distMatrix[temp_vertices[a], temp_vertices[a+1]]
            c_ik=distMatrix[temp_vertices[a], selected]
            c_kj=distMatrix[selected, temp_vertices[a+1]]
            arc_candid.append(c_ik+c_kj-c_ij)
        index = np.argmin(arc_candid)+1
        tour_new.insert(index, selected)
    return tour_new,chosen_candid

def deletion(tour, distMatrix,profits): #deletion operator
    tour_new = tour.copy()
    chosen_candid = None
    if len(tour) >= 1: # do not delete when there is less than 2 cities in the tour
        candidates = []
        candidates = tour.copy()
        candidates.remove(0) #remove depot from the tour
        gain_candidates = []

        # choose the best candidate with roulette wheel selection method where selection probabilities
        # are proportional to their contribution(=profit loss/distance reduction)
        for candid in candidates:
            profit_candid = profits[candid]
            idx = tour.index(candid)

            if candid != tour[-1]:  # if candidate is not the last city on tour
                d_ij = distMatrix[tour[idx-1], tour[idx]]
                d_ik = distMatrix[tour[idx-1], tour[idx+1]]
                d_jk = distMatrix[tour[idx], tour[idx + 1]]

            else:
                idx=-1
                d_ij = distMatrix[tour[idx - 1], tour[idx]]
                d_ik = distMatrix[tour[idx - 1], tour[idx + 1]]
                d_jk = distMatrix[tour[idx], tour[idx + 1]]

            distance_gain = -(d_ik - d_ij - d_jk)
            # calculate gain of deletion
            total_gain = -profit_candid / (distance_gain + 0.0001)

            gain_candidates.append(total_gain)

        minimum = min(gain_candidates)
        adding = 0
        if minimum<0:
            adding = -minimum
        gain_candidates = [x+adding for x in gain_candidates]

        # chose the candidate city to delete via roulette wheel selection method
        chosen_candid = candidates[roulette(gain_candidates)]

        #delete the chosen candidate from the tour
        tour_new.remove(chosen_candid)

    return tour_new,chosen_candid

#decide whether deletion or insertion is more profitable
def chooseBestAction(tour, distMatrix, profits,a,incumbentObj,tabuList):
    #calculate objective function values of actions
    tour_old = tour.copy()
    tourDeleted, citytobeDeleted = deletion(tour_old,distMatrix,profits)
    tourInserted, citytobeInserted = insertion(tour_old,distMatrix,profits,a)
    insertionObj = calculateObjective(tourInserted,distMatrix,profits)
    deletionObj = calculateObjective(tourDeleted,distMatrix,profits)

    if citytobeInserted == None:      # when there is no city left to be inserted, implemement deletion
        ##Problem: insert edilecek adam kalmazsa sil, böyle olunca silinemez tabusundaki nodu silmiş olabiliriz ve sonraki iterationda karar ekleme ise tekrar tabuya koyabiliriz
        tour_new = tourDeleted.copy()

    elif citytobeDeleted == None: # when deletion is not permitted, perform insertion and add the inserted city to tabu list
        tour_new = tourInserted.copy()
        tabuList[citytobeInserted] = np.random.randint(10, 20)

    else:
        # if deletion if it gives better obj value than insertion
        if deletionObj>insertionObj:
            # check if it gives better than incumbent or not in tabu list, then implement deletion
            # else do not perform deletion
            if deletionObj>incumbentObj or citytobeDeleted not in tabuList.keys():
                tour_new = tourDeleted.copy()
            else:
                tour_new = tour.copy()
        # if deletion does not give better obj value than insertion and inserted city is not in the tabu list
        # perform insertion and update the tabu list
        elif citytobeInserted not in tabuList.keys():
            tour_new = tourInserted.copy()
            tabuList[citytobeInserted] = np.random.randint(10, 20)
        # if all fails, do not make any change to the tour
        else:
            tour_new = tour

    return tour_new

def threeOptSwap(route, i, j, k):
    bestRoute = list(route)
    best_diff = 0

    a = i
    b = j + 1
    c = k + 2

    nRoute = route[:a] + list(reversed(route[a:b])) + list(reversed(route[b:c])) + route[c:]
    diff = distances[route[a - 1]][route[a]] + distances[route[b - 1]][route[b]] + distances[route[c - 1]][route[c]]
    diff = diff - distances[route[a - 1]][route[b - 1]] - distances[route[a]][route[c - 1]] - distances[route[b]][
        route[c]]
    if diff > best_diff:
        best_diff = diff
        bestRoute = list(nRoute)

    nRoute = route[:a] + route[b:c] + route[a:b] + route[c:]
    diff = distances[route[a - 1]][route[a]] + distances[route[b - 1]][route[b]] + distances[route[c - 1]][route[c]]
    diff = diff - distances[route[a - 1]][route[b]] - distances[route[c - 1]][route[a]] - distances[route[b - 1]][
        route[c]]
    if diff > best_diff:
        best_diff = diff
        bestRoute = list(nRoute)

    nRoute = route[:a] + route[b:c] + list(reversed(route[a:b])) + route[c:]
    diff = distances[route[a - 1]][route[a]] + distances[route[b - 1]][route[b]] + distances[route[c - 1]][route[c]]
    diff = diff - distances[route[a - 1]][route[b]] - distances[route[c - 1]][route[b - 1]] - distances[route[a]][
        route[c]]
    if diff > best_diff:
        best_diff = diff
        bestRoute = list(nRoute)

    nRoute = route[:a] + list(reversed(route[b:c])) + route[a:b] + route[c:]
    diff = distances[route[a - 1]][route[a]] + distances[route[b - 1]][route[b]] + distances[route[c - 1]][route[c]]
    diff = diff - distances[route[a - 1]][route[c - 1]] - distances[route[b]][route[a]] - distances[route[b - 1]][
        route[c]]
    if diff > best_diff:
        best_diff = diff
        bestRoute = list(nRoute)

    return bestRoute, best_diff


def threeOpt(route):
    xx = 0
    while (True):
        xx += 1
        temp_route = list(route)
        old_route = list(route)
        best_diff = 0.01
        brk = False
        li = list(range(1, len(route) - 2))
        random.shuffle(li)
        for i in li:
            lj = list(range(i, len(route) - 2))
            random.shuffle(lj)
            for j in lj:
                lk = list(range(j, len(route) - 2))
                random.shuffle(lk)
                for k in lk:
                    new_route, new_diff = threeOptSwap(route, i, j, k)
                    if new_diff > best_diff:
                        temp_route = list(new_route)
                        best_diff = new_diff
                        brk = True
                        break
                if brk:
                    break
            if brk:
                break
        if not brk:
            break
        if best_diff > 0.01:
            route = list(temp_route)
        else:
            break
    return route

#MAIN KISMI


tours,tours_after2opt,initial_objectives,initial_objectives_after2opt = initialization(distances,profits,starttime = starttime,construction_method="nearest neighbor",nstart=10)
tour = tours[4]   #burada random da seçilebilir

tour = shift(tour)

tabuList = {}
incumbentSoln = tour.copy()
incumbentObj = calculateObjective(incumbentSoln,distances,profits)

# Start the timer
start = time.time()

count = 0
for i in range(iterations):
    a = i
    tour = chooseBestAction(tour, distances, profits,a,incumbentObj,tabuList)

    tour = two_opt(tour,distances)
    currentObj = calculateObjective(tour,distances,profits)
    if currentObj > incumbentObj:
        count = 0
        #tour = threeOpt(tour)  #3-OPT OPSIYONEL, SUYUNUNUN FONKSIYONU DIREKT
        currentObj = calculateObjective(tour,distances,profits)
        incumbentObj = currentObj
        incumbentSoln = tour.copy()
    else:
        count += 1
    #UPDATE THE TABU LIST
    for key, value in list(tabuList.items()):
            v = tabuList
            tabuList[key] = tabuList[key] - 1
            if tabuList[key] == 0:
                del(tabuList[key])
    '''
    #SHAKING KISMI (OPSIYONEL)
    if count == 40:
        random.shuffle(tour)
        tabuList.clear()
    '''

print(incumbentSoln)
print(incumbentObj)

timediff = time.time() - starttime
print("Total CPU time: " + str(timediff))
print("Number of cities visited: (with depot):" + str(len(incumbentSoln)))

#BYFAR BEST FOR 51-HP           704.84    CPU TIME: 50.36
#print(str(calculateObjective([0, 31, 10, 37, 4, 36, 16, 3, 17, 46, 11, 45, 50, 26, 5, 47, 22, 6, 42, 23, 13, 24, 12, 40,18, 41, 43, 14, 44, 32, 38, 9, 48, 8, 29, 33, 20, 49, 15, 1, 28, 19, 34, 35, 2, 27, 30, 25, 7, 21],distances,profits)))

#BYFAR BEST FOR 51-LP            50.75     CPU TIME: 112.02
#print(str(calculateObjective([0, 47, 5, 13, 17, 3, 16, 43, 14, 44, 32, 9, 29, 33, 49, 15, 8, 37, 4, 45, 50, 31],distances,profits)))

#BYFAR BEST FOR 76-HP        1245.63     CPU TIME: 158.51   75 cities+depot
#print(str(calculateObjective([0, 32, 62, 15, 2, 43, 31, 8, 38, 71, 57, 11, 39, 16, 50, 5, 67, 3, 74, 75, 25, 66, 33, 45, 51, 26, 44, 28, 47, 29, 1, 73, 27, 60, 20, 46, 35, 68, 70, 59, 69, 19, 36, 4, 14, 56, 12, 53, 18, 7, 34, 6, 52, 13, 58, 10, 65, 64, 37, 9, 30, 54, 24, 49, 17, 23, 48, 22, 55, 40, 42, 41, 63, 21, 61, 72],distances,profits)))

#BYFAR BEST FOR 101-LP       262.53    75 cities+depot
#print(str(calculateObjective([0, 68, 30, 87, 61, 9, 31, 89, 62, 10, 18, 46, 47, 81, 6, 17, 82, 59, 4, 83, 60, 15, 85, 43, 90, 99, 84, 92, 97, 36, 91, 58, 98, 95, 5, 93, 12, 94, 96, 86, 41, 42, 14, 56, 40, 21, 73, 74, 55, 22, 38, 3, 71, 72, 20, 39, 57, 52, 100, 27, 11, 79, 67, 23, 28, 77, 33, 34, 8, 50, 32, 78, 2, 76, 75, 49],distances,profits)))