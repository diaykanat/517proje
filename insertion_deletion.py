#function to argsort for lists
#thank you stackoverflow
def h(seq):
    return sorted(range(len(seq)), key=seq.__getitem__)

def insertion(tour,distMatrix,profits):
    tour_new = tour.copy()
    chosen_candid = None
    if len(tour)!=len(distMatrix):
        candidates = [] #bunu yapmayinca sapitti nedense
        candidates = [i for i in range(51) if i not in tour]
        gain_candidates = []

        # choose the best candidate randomly among best 2 candidates according to
        # their contribution(=profit-distance to nearest city in the tour)
        for candid in candidates:
            profit_candid = profits[candid]
            coord_candid = np.array(dat.iloc[candid][['x','y']]).reshape(1,-1)
            coord_tour = np.array(dat.iloc[tour][['x','y']])
            dist_to_tour = distance_matrix(coord_candid,coord_tour,p=2)
            gain_candid = np.max(profit_candid - dist_to_tour)
            gain_candidates.append(gain_candid)
        best2_indices = [h(gain_candidates)[-1],h(gain_candidates)[-2]]
        chosen_candid = candidates[np.random.choice(best2_indices, 1).tolist()[0]]

        #insert the chosen candidate to the tour
        temp_vertices = tour+[tour[0]]
        arc_candid = []
        selected = chosen_candid

        #find all possible insertions and their costs=(increase in distance)
        for a in range(len(temp_vertices)-1):
            c_ij=distMatrix[temp_vertices[a], temp_vertices[a+1]]
            c_ik=distMatrix[temp_vertices[a], selected]
            c_kj=distMatrix[selected, temp_vertices[a+1]]
            arc_candid.append(c_ik+c_kj-c_ij)
        index = np.argmin(arc_candid)+1
        tour_new.insert(index, selected)

    return tour_new,chosen_candid

def deletion(tour, distMatrix,profits):
    tour_new = tour.copy()
    chosen_candid = None
    if len(tour)!=0:
        candidates = []
        candidates = tour.copy()
        gain_candidates = []

        # choose the best candidate randomly among best 2 candidates according to
        # their contribution(=-profit+distance reduction)
        for candid in candidates:
            profit_candid = profits[candid]
            idx = tour.index(candid)

            if candid != tour[-1]:
                d_ij = distMatrix[tour[idx-1], tour[idx]]
                d_ik = distMatrix[tour[idx-1] , tour[idx+1]]
                d_jk = distMatrix[tour[idx], tour[idx + 1]]

            else:
                idx=-1
                d_ij = distMatrix[tour[idx - 1], tour[idx]]
                d_ik = distMatrix[tour[idx - 1], tour[idx + 1]]
                d_jk = distMatrix[tour[idx], tour[idx + 1]]

            distance_gain = -(d_ik - d_ij - d_jk)
            total_gain = -profit_candid+distance_gain
            gain_candidates.append(total_gain)

        best2_indices = [h(gain_candidates)[-1],h(gain_candidates)[-2]]
        chosen_candid = candidates[np.random.choice(best2_indices, 1).tolist()[0]]
        #delete the chosen candidate from the tour
        tour_new.remove(chosen_candid)

    return tour_new,chosen_candid

#deletion(tours[2], distances, profits)

##### tours da degisiyor nasil engelleriz? tour = tours[2] yapinca da olmadi.

#tour=tours[0]
#distMatrix=distances.copy()

#bu asagilar hep dutluk olacak
#main solver calismadan yazmak gerek, su an bunlar olmazsa fonksiyon calismaz
tabuList = {}
incumbentSoln = list(tours[2])
incumbentObj = calculateObjective(incumbentSoln,distances,profits)

def chooseBestAction(tour, distMatrix, profits):
    #calculate objective function values of actions
    tour_old = tour.copy()
    tourDeleted, citytobeDeleted = deletion(tour_old,distMatrix,profits)
    tourInserted, citytobeInserted = insertion(tour_old,distMatrix,profits)
    insertionObj = calculateObjective(tourInserted,distMatrix,profits)
    deletionObj = calculateObjective(tourDeleted,distMatrix,profits)

    if deletionObj>insertionObj: #implement deletion
        if deletionObj>incumbentObj or citytobeDeleted not in tabuList.keys():
            tour_new = tourDeleted.copy()
    else: #implement insertion
        tour_new = tourInserted.copy()
        print(tourInserted)
        #update tabu_list
        tabuList[citytobeInserted] = np.random.randint(10, 20)
        #tabu listesinin her iterasyonda guncellenmesi lazim. suyunuya bakabiliriz.

    return tour_new

print(tours[4])
chooseBestAction(tours[4], distances, profits)



# Iteration Count
ITER = 10000

# Start the timer
start = time.time()

# Create the initial route
#tour = initialization()[np.random.randint(0,10,1)[0]]

# Determine what move to apply (insertion or deletion)
#tour = chooseBestAction()
#tabuList = {}
#solutionIndex = [0] bu ne ise yariyor anlamadim?

#incumbentSoln = list(tour)
#incumbentObj = calculateObjective(tour,distances,profits)







